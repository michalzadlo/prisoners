#!/bin/sh

ssh -tt api-deployer@arantio.tshdev.io "
    cd webinar
    sed -i -e '/^DATA_TAG/ d' .env && echo 'DATA_TAG=$BITBUCKET_COMMIT' >> .env;
    docker-compose build php
    docker-compose up --no-deps -d php
"