<?php declare(strict_types=1);

namespace Prisoners\Application\User\Command;

use Prisoners\Domain\Model\User\UserId;

final class CreateUserCommand
{
    /**
     * @var UserId
     */
    public $userId;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $roleName;
}
