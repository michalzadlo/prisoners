<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\PublicPrisonersList;

use Prisoners\Domain\Model\Prisoner\PrisonerId;

interface PublicPrisonersListRepository
{
    public function fetchAll(): array;

    public function save(PublicPrisonersListItem $item): void;

    public function delete(PrisonerId $prisonerId): void;
}
