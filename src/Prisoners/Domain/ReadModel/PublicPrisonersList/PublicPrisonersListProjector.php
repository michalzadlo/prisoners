<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\PublicPrisonersList;

use Broadway\ReadModel\Projector;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasReleasedEvent;
use Prisoners\Domain\Model\Prisoner\Event\PrisonerWasAddedEvent;

final class PublicPrisonersListProjector extends Projector
{
    /**
     * @var PublicPrisonersListRepository
     */
    private $publicListRepository;

    public function __construct(PublicPrisonersListRepository $publicListRepository)
    {
        $this->publicListRepository = $publicListRepository;
    }

    protected function applyPrisonerWasAddedEvent(PrisonerWasAddedEvent $prisonerWasAddedEvent): void
    {
        $publicListItem = new PublicPrisonersListItem();
        $publicListItem->name = sprintf('%s %s', $prisonerWasAddedEvent->lastName, $prisonerWasAddedEvent->firstName);
        $publicListItem->crimeCategory = $prisonerWasAddedEvent->crimeCategory;
        $publicListItem->startDate = $prisonerWasAddedEvent->startDate;
        $publicListItem->endDate = $prisonerWasAddedEvent->endDate;
        $publicListItem->id = $prisonerWasAddedEvent->id->get();

        $this->publicListRepository->save($publicListItem);
    }

    protected function applyPrisonerWasReleasedEvent(PrisonerWasReleasedEvent $prisonerWasReleasedEvent): void
    {
        $this->publicListRepository->delete($prisonerWasReleasedEvent->prisonerId);
    }
}
