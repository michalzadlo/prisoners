<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Penitentiary;

use Broadway\ReadModel\Projector;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasAssignedEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasChangedCellEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasReleasedEvent;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;
use Prisoners\Domain\Model\Prisoner\PrisonerRepository;

final class PrisonersListProjector extends Projector
{
    /**
     * @var PenitentiaryPrisonersListRepository
     */
    private $prisonersListRepository;

    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    /**
     * @var PrisonerRepository
     */
    private $prisonerRepository;

    public function __construct(
        PenitentiaryPrisonersListRepository $prisonersListRepository,
        PenitentiaryRepository $penitentiaryRepository,
        PrisonerRepository $prisonerRepository
    ) {
        $this->prisonersListRepository = $prisonersListRepository;
        $this->penitentiaryRepository = $penitentiaryRepository;
        $this->prisonerRepository = $prisonerRepository;
    }

    protected function applyPrisonerWasAssignedEvent(PrisonerWasAssignedEvent $prisonerWasAssignedEvent): void
    {
        $penitentiary = $this->penitentiaryRepository->get($prisonerWasAssignedEvent->penitentiaryId);
        $prisoner = $this->prisonerRepository->get($prisonerWasAssignedEvent->prisonerId);

        $prisonerReadModel = new Prisoner(
            $prisonerWasAssignedEvent->prisonerId->get(),
            sprintf('%s %s', $prisoner->getPerson()->getLastName(), $prisoner->getPerson()->getFirstName()),
            $prisonerWasAssignedEvent->penitentiaryId->get(),
            $penitentiary->getName(),
            $prisonerWasAssignedEvent->block,
            $prisonerWasAssignedEvent->cellNumber,
            $prisoner->getDurationOfStay()->getStartDate(),
            $prisoner->getDurationOfStay()->getEndDate()
        );

        $this->prisonersListRepository->save($prisonerReadModel);
    }

    protected function applyPrisonerWasChangedCellEvent(PrisonerWasChangedCellEvent $prisonerWasChangedCellEvent): void
    {
        $penitentiary = $this->penitentiaryRepository->get($prisonerWasChangedCellEvent->penitentiaryId);
        $prisoner = $this->prisonerRepository->get($prisonerWasChangedCellEvent->prisonerId);

        $prisonerReadModel = new Prisoner(
            $prisonerWasChangedCellEvent->prisonerId->get(),
            sprintf('%s %s', $prisoner->getPerson()->getLastName(), $prisoner->getPerson()->getFirstName()),
            $prisonerWasChangedCellEvent->penitentiaryId->get(),
            $penitentiary->getName(),
            $prisonerWasChangedCellEvent->block,
            $prisonerWasChangedCellEvent->cellNumber,
            $prisoner->getDurationOfStay()->getStartDate(),
            $prisoner->getDurationOfStay()->getEndDate()
        );

        $this->prisonersListRepository->save($prisonerReadModel);
    }

    protected function applyPrisonerWasReleasedEvent(PrisonerWasReleasedEvent $prisonerWasReleasedEvent): void
    {
        $this->prisonersListRepository->delete($prisonerWasReleasedEvent->prisonerId);
    }
}
