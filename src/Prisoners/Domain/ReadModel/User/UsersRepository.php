<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\User;

use Symfony\Component\Security\Core\User\UserInterface;

interface UsersRepository
{
    public function findByUsername(string $username): User;

    public function save(UserInterface $item): void;
}
