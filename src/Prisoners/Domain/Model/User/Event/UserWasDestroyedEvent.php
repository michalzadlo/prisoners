<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\User\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\User\UserId;

final class UserWasDestroyedEvent implements Serializable
{
    /**
     * @var UserId
     */
    public $userId;

    public function __construct(UserId $id)
    {
        $this->userId = $id;
    }

    public static function deserialize(array $data): self
    {
        return new self(
            new UserId($data['user_id'])
        );
    }

    public function serialize(): array
    {
        return [
            'user_id' => $this->userId->get(),
        ];
    }
}
