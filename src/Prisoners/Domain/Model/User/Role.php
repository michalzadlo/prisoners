<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\User;

final class Role
{
    /**
     * @var string
     */
    private $name;

    /**
     * Role constructor.
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
