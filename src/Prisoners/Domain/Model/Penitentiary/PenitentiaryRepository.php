<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary;

interface PenitentiaryRepository
{
    public function get(PenitentiaryId $userId): ?Penitentiary;

    public function save(Penitentiary $user): void;
}
