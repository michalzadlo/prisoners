<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Prisoner;

interface PrisonerRepository
{
    public function get(PrisonerId $prisonerId): ?Prisoner;

    public function save(Prisoner $prisoner): void;
}
