<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel\User;

use Prisoners\Domain\ReadModel\User\User;
use Prisoners\Domain\ReadModel\User\UsersRepository;
use Prisoners\Infrastructure\ReadModel\AbstractRepository;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserBroadwayRepository extends AbstractRepository implements UsersRepository
{
    public function save(UserInterface $user): void
    {
        $this->getRepository()->save($user);
    }

    public function findByUsername(string $username): User
    {
        // TODO change to findBy
        $users = $this->getRepository()->findAll();

        /** @var User $user */
        foreach ($users as $user) {
            if ($user->getUsername() === $username) {
                return $user;
            }
        }

        return null;
    }

    protected function getName(): string
    {
        return 'user_repository';
    }

    protected function getClass(): string
    {
        return User::class;
    }
}
