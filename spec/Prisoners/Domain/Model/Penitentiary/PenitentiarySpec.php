<?php

namespace spec\Prisoners\Domain\Model\Penitentiary;

use PhpSpec\ObjectBehavior;
use Prisoners\Domain\Model\Penitentiary\Penitentiary;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;

class PenitentiarySpec extends ObjectBehavior
{
    public function it_creates_a_penitentiary(): void
    {
        $this->beConstructedThrough('create', [ new PenitentiaryId('zkwojkowice'),'ZK Wojkowice']);
        $this->shouldBeAnInstanceOf(Penitentiary::class);
        $this->getAggregateRootId()->shouldReturn('zkwojkowice');
    }
}
